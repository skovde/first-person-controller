# First Person Controller
Add the following to your `manifest.json`-file:

```json
    "se.his.dsu.fpscontroller": "https://bitbucket.org/skovde/first-person-controller.git",
```